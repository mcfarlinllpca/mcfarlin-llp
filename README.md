McFarlin LLP provides legal services to small to mid-sized businesses & property owners in California. Experienced in business litigation & real estate litigation. McFarlin LLP has a renowned bankruptcy and foreclosure defense practice, including bankruptcy litigation.

Address: 11 Mareblu, Suite 100-A, Aliso Viejo, CA 92656, USA

Phone: 949-544-3052

Website: http://www.mcfarlinlaw.com